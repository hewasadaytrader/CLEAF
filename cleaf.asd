;;;; cleaf.asd

(in-package :asdf-user)

(defsystem #:cleaf
  :description "A library of somewhat related projects that encompase financial and economic calculations"
  :version "0.0.1"
  :author "William Glenn <pierceglenn@hotmail.com>"
  :license "MIT"
  :depends-on (#:lisp-stat)
  :components ((:file "finance/time-value-money")))
