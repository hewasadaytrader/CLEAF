;;;; time-value-money.lisp

(defun intr (infl rtrn)
  "This replicates the process required to 'normalize'(not the right term?) the inflation and rate of returns"
  (declare (optimize (speed 3) (safety 0)))
  (- (/ (+ 1 rtrn)
        (+ 1 infl))
     1))

(defun fv (pv intr yrs &optional (n 1))
  "future value"
  (declare (optimize (speed 3) (safety 0)))
  (* pv (expt
	 (+ 1 (/ intr n))
	 (* n yrs))))

(defun pv (fv intr yrs &optional (n 1))
  "present value"
  (declare (optimize (speed 3) (safety 0)))
  (/ fv (expt
	 (+ 1 (/ intr n))
	 (* n yrs))))

(defun pva (pmt intr yrs)
  "present value of an annuity"
  (declare (optimize (speed 3) (safety 0)))
  (* pmt (/ (- 1 (expt (+ 1 intr)
		       (* -1 yrs)))
	    intr)))

(defun fva (pmt intr yrs)
  "future value of an annuity"
  (declare (optimize (speed 3) (safety 0)))
  (* pmt (/ (- (expt (+ 1 intr) yrs)
	       1)
	    intr)))

(defun pva-pmt (prva intr yrs)
  "payments calculation on a present value of an annuity"
  (declare (optimize (speed 3) (safety 0)))
  (/ (* prva intr)
     (- 1
	(expt (+ 1 intr) (* -1 yrs)))))

(defun pvad (pmt intr yrs)
  "present value of annuity due"
  (declare (optimize (speed 3) (safety 0)))
  (* pmt (* (/ (- 1
		  (/ 1
		     (expt (+ 1 intr) yrs)))
	       intr)
	    (+ 1 intr))))

(defun fvad (pmt intr yrs)
  "future value of annuity due"
  (declare (optimize (speed 3) (safety 0)))
  (* pmt
     (- (expt (+ 1 intr) yrs)
	1)
     (/ (+ 1 intr)
	intr)))

    
;;EOF
